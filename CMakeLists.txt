cmake_minimum_required(VERSION 3.2)

project("LibraryProgram" VERSION 1.0)

set(SOURCE LibraryProgram1.cpp)

add_executable("${PROJECT_NAME}" "${SOURCE}")

