#include<fstream>
#include<stdio.h>
#include<string.h>
#include<iomanip>
#include<iostream>
#include<cstdlib>
#include<cstdio>
//***************************************************************
//                   CLASS USED IN PROJECT
//****************************************************************
class book
{
        char bId[6];
        char bname[50];
        char aname[20];
  public:
    void show_book()
    {
        std::cout<<"\nBook no. : "<<bId;
        std::cout<<"\nBook Name : ";
        puts(bname);
        std::cout<<"Author Name : ";
        puts(aname);
    }
    char* retbId()
    {
        return bId;
    }
    void report()
    {
        std::cout<<bId<<std::setw(30)<<bname<<std::setw(30)<<aname<<std::endl;
        }
};         //class ends here
//***************************************************************
//        global declaration for stream object, object
//****************************************************************
//fstream fp,fp1;
book bk;
//***************************************************************
//        function to read specific record from file
//****************************************************************
void display_spb(char n[])
{
    std::cout<<"\nBOOK DETAILS\n";
    int flag=0;
    std::fstream fp;
    fp.open("book.dat",std::ios::in);
    while(fp.read((char*)&bk,sizeof(book)))
    {
        if(strcmp(bk.retbId(),n)==0)
        {
            bk.show_book();
             flag=1;
        }
    }
    fp.close();
    if(flag==0)
        std::cout<<"\n\nBook does not exist";
}
//***************************************************************
//        function to display Books list
//****************************************************************
void display_allb()
{
    std::fstream fp;
    fp.open("book.dat",std::ios::in);
    if(!fp)
    {
        std::cout<<"ERROR!!! FILE COULD NOT BE OPEN ";
               return;
    }
    std::cout<<"\n\n\t\tBook LIST\n\n";
    std::cout<<"=========================================================================\n";
    std::cout<<"Book Number"<<std::setw(20)<<"Book Name"<<std::setw(25)<<"Author\n";
    std::cout<<"=========================================================================\n";
    while(fp.read((char*)&bk,sizeof(book)))
    {
        bk.report();
    }
         fp.close();
}
//***************************************************************
//        ADMINISTRATOR MENU FUNCTION
//****************************************************************
void admin_menu()
{
    int ch2;
    std::cout<<"\n\n\t1.DISPLAY ALL BOOKS ";
    std::cout<<"\n\n\t2.DISPLAY SPECIFIC BOOK ";
    std::cout<<"\n\n\t03. EXIT";
    std::cout<<"\n\n\tPlease Enter Your Choice (1-3) ";
    std::cin>>ch2;
    switch(ch2)
    {
        case 1: display_allb();break;
        case 2: {
                   char num[6];
                   std::cout<<"\n\n\tPlease Enter The book No. ";
                   std::cin>>num;
                   display_spb(num);
                   break;
            }
        case 3:exit(0);
            default:std::cout<<"\a";
       }
       admin_menu();
}
//***************************************************************
//        THE MAIN FUNCTION OF PROGRAM
//****************************************************************
int main()
{
    char ch;
    do
    {
          std::cout<<"\n\n\t01. ADMINISTRATOR MENU";
          std::cout<<"\n\n\t02. EXIT";
          std::cout<<"\n\n\tPlease Select Your Option (1-2) ";
          std::cin>>ch;
          switch(ch)
          {
              case '1':admin_menu();
                 break;
              case '2':exit(0);
              default :std::cout<<"\a";
        }
        }while(ch!='2');
    return 0;
}
